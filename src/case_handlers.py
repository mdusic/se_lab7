import os

class CaseError(Exception):
    """Exception raised for incorrect case handling

    Attributes: 
        path -- input path that caused the exception
        message -- explanation of the error
        error_code -- error code to return
    """

    def __init__(self, path, message, error_code):
        self.path = path
        self.message = message
        self.error_code = error_code


class CaseFileExistsHandler:
    def test(self, file_path):
        if os.path.isfile(file_path):
            return True
        else:
            return False
    def run(self, file_path):
        return FileReader().read(file_path)

class CaseFileNotExistsHandler:
    def test(self, file_path):
        if not os.path.isfile(file_path) and not os.path.isdir(file_path):
            return True
        else:
            return False

    def run(self, file_path):
        raise CaseError(file_path, "File not found", 404)

class CaseRootPathHandler():
    def test(self, file_path):
        if file_path[-1] == "/":
            return True
        

    def run(self, file_path):
        file_path += "index.html"
        return FileReader().read(file_path)

class FileReader():
    def read(self, file_path):
        try: 
            with open(file_path, "rb") as reader:
                content = reader.read()
            return content
        except Exception:
            raise CaseError(file_path, "Internal server Error", 500)
