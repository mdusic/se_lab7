import pytest
from ..src.case_handlers import *

def get_path(file_path):
    full_path = "/home/osirv/pi_labs/Dusic/se_lab7/se_lab7/tests/test_assets/"
    return full_path + file_path

def test_file_exists_true():
    file_path = get_path("file.html")
    handler = CaseFileExistsHandler()
    assert handler.test(file_path) is True
def test_file_exists_false():
    file_path = get_path("nepostoji.html")
    handler = CaseFileExistsHandler()
    assert handler.test(file_path) is False

def test_file_exists_run_true():
    file_path = get_path("file.html")
    handler = CaseFileExistsHandler()
    assert "This file exists" in handler.run(file_path)
def test_file_exists_run_false():
    file_path = get_path("nepostoji.html")
    handler = CaseFileExistsHandler()
    with pytest.raises(CaseError) as e:
        handler.run(file_path)
    assert e.value.error_code == 500

def test_file_not_exists_true():
    file_path = get_path("nepostoji.html")
    handler = CaseFileNotExistsHandler()
    assert handler.test(file_path) is True

def test_file_not_exists_false():
    file_path = get_path("file.html")
    handler = CaseFileNotExistsHandler()
    assert handler.test(file_path) is False